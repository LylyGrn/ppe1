<?php
/** 
 * Script de contrôle et d'affichage du cas d'utilisation "Consulter la liste des fiches de frais"
 * @author Lysandra Grenier
 */
  $repInclude = './include/';
  require($repInclude . "_init.inc.php");

  // page inaccessible si l'utilisateur n'est pas connecté
  if ( ! estUtilisateurConnecte() ) {
      header("Location: cSeConnecter.php");  
  }
  
  // acquisition des données entrées, ici l'id de visiteur, le mois et l'étape du traitement
  /**
   * $idVisiteur permet de récupérer le premier visiteur
   * $idMois permet de récupérer le 1er mois 
   */
$idVisiteur = lireDonnee("lstVisiteur", "");
$idMois = lireDonnee("lstMois", "");
  
  require($repInclude . "_entete.inc.html");
  require($repInclude . "_sommaire.inc.php");
  
 

?>
<!-- Division principale -->
<div id="contenu">
    <?php
    $lgVisiteur = obtenirDetailUtilisateur($idConnexion, $idVisiteur);
    $noMois = intval(substr($idMois, 4, 2));
    $annee = intval(substr($idMois, 0, 4));

    
    ?>
    <h1>Liste de toutes les fiches de frais</h1>
    <?php
    $req = "SELECT utilisateur.id, nom, prenom, ficheFrais.mois, SUM(lignefraisforfait.quantite * fraisForfait.montant) AS montantForfait,";
    $req .= " (ficheFrais.montantValide - SUM(lignefraisforfait.quantite * fraisForfait.montant)) AS montantHorsForfait, ficheFrais.montantValide AS totalFicheFrais";
    $req .= " FROM utilisateur INNER JOIN ficheFrais ON utilisateur.id=ficheFrais.idVisiteur";
    $req .= "                  INNER JOIN lignefraisforfait ON (ficheFrais.idVisiteur = lignefraisforfait.idVisiteur  AND ficheFrais.mois = lignefraisforfait.mois)";
    $req .= "                  INNER JOIN fraisForfait ON lignefraisforfait.idFraisForfait = fraisForfait.id";
    $req .= " GROUP BY nom, prenom, ficheFrais.mois";
    $idListeFiche = mysqli_query($idConnexion,$req);
    ?>

<div><h2>Fiches de frais par utilisateur</h2></div>
    <table class="colortable" border="1">
        <tr><th rowspan="2" style="vertical-align:middle;">Visiteur&nbsp;médical</th><th rowspan="2" style="vertical-align:middle;">Mois</th><th colspan="3">Fiches de frais</th></tr>
        <tr><th>Forfait</th><th>Hors forfait</th><th>Total</th></tr>
        <?php
            while ($ListeFiche = mysqli_fetch_array($idListeFiche)) {
                $mois = $ListeFiche['mois'];
                $noMois = intval(substr($mois, 4, 2));
                $annee = intval(substr($mois, 0, 4));
        ?>
        <tr align="center">
            <td style="width:80px;white-space:nowrap;color:black;" class = "tdgsb"><?php echo $ListeFiche['nom'] . ' ' . $ListeFiche['prenom']; ?></td>
            <td style="width:80px;white-space:nowrap;color:black;"><?php echo obtenirLibelleMois($noMois) . ' ' . $annee; ?></td>
            <td style="width:80px;white-space:nowrap;color:black;text-align:right;"><?php echo $ListeFiche['montantForfait']; ?></td>
            <td style="width:80px;white-space:nowrap;color:black;text-align:right;"><?php echo $ListeFiche['montantHorsForfait']; ?></td>
            <td style="width:80px;white-space:nowrap;color:black;text-align:right;"><?php echo $ListeFiche['totalFicheFrais']; ?></td>
        </tr>

                <?php
            }
            ?>
        </table>
</div>
<?php
require($repInclude . "_pied.inc.html");
require($repInclude . "_fin.inc.php");
?>