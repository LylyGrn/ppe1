-- phpMyAdmin SQL Dump
-- version 3.3.9
-- http://www.phpmyadmin.net
--
-- Serveur: localhost
-- Généré le : Lun 04 Juillet 2011 à 14:08
-- Version du serveur: 5.5.8
-- Version de PHP: 5.3.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `gsb_frais`
--
CREATE DATABASE gsb_valide;
USE gsb_valide;
-- --------------------------------------------------------

--
-- Structure de la table `FraisForfait`
--

CREATE TABLE IF NOT EXISTS `FraisForfait` (
  `id` char(3) NOT NULL,
  `libelle` char(20) DEFAULT NULL,
  `montant` decimal(5,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;


-- --------------------------------------------------------

--
-- Structure de la table `Etat`
--

CREATE TABLE IF NOT EXISTS `Etat` (
  `id` varchar(2) NOT NULL,
  `libelle` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- --------------------------------------------------------

--
-- Structure de la table `Visiteur`
--

CREATE TABLE IF NOT EXISTS `Visiteur` (
  `id` varchar(4) NOT NULL,
  `nom` varchar(30) DEFAULT NULL,
  `prenom` varchar(30)  DEFAULT NULL, 
  `login` varchar(20) DEFAULT NULL,
  `mdp` varchar(255) DEFAULT NULL,
  `adresse` varchar(30) DEFAULT NULL,
  `cp` varchar(5) DEFAULT NULL,
  `ville` varchar(30) DEFAULT NULL,
  `dateEmbauche` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;


-- --------------------------------------------------------

--
-- Structure de la table `FicheFrais`
--

CREATE TABLE IF NOT EXISTS `fichefrais` (
  `idVisiteur` varchar(4) NOT NULL,
  `mois` varchar(6) NOT NULL,
  `nbJustificatifs` int(11) DEFAULT NULL,
  `montantValide` decimal(10,2) DEFAULT NULL,
  `dateModif` date DEFAULT NULL,
  `idEtat` varchar(2) DEFAULT 'CR',
  PRIMARY KEY (`idVisiteur`,`mois`),
  FOREIGN KEY (`idEtat`) REFERENCES Etat(`id`),
  FOREIGN KEY (`idVisiteur`) REFERENCES Visiteur(`id`)
) ENGINE=InnoDB;


-- --------------------------------------------------------

--
-- Structure de la table `LigneFraisForfait`
--

CREATE TABLE IF NOT EXISTS `LigneFraisForfait` (
  `idVisiteur` varchar(4) NOT NULL,
  `mois` varchar(6) NOT NULL,
  `idFraisForfait` varchar(3) NOT NULL,
  `quantite` int(11) DEFAULT NULL,
  PRIMARY KEY (`idVisiteur`,`mois`,`idFraisForfait`),
  FOREIGN KEY (`idVisiteur`, `mois`) REFERENCES FicheFrais(`idVisiteur`, `mois`),
  FOREIGN KEY (`idFraisForfait`) REFERENCES FraisForfait(`id`)
) ENGINE=InnoDB;

-- --------------------------------------------------------

--
-- Structure de la table `LigneFraisHorsForfait`
--

CREATE TABLE IF NOT EXISTS `LigneFraisHorsForfait` (
  `id` int(11) NOT NULL auto_increment,
  `idVisiteur` varchar(4) NOT NULL,
  `mois` varchar(6) NOT NULL,
  `libelle` varchar(100) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `montant` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (`idVisiteur`, `mois`) REFERENCES FicheFrais(`idVisiteur`, `mois`)
) ENGINE=InnoDB;

--
-- Table `type`
--
CREATE TABLE `type` (
  id varchar(1),
  libelleType varchar(30),
  CONSTRAINT pk_type PRIMARY KEY (id)
) ENGINE=InnoDB;

--
-- Table `typeVehicule`
--
CREATE TABLE `typeVehicule` (
  id varchar(10),
  libelle varchar(60),
  montant float,  
  CONSTRAINT pk_typeVehicule PRIMARY KEY (id)
) ENGINE=InnoDB;

--
-- Changement de nom de table
--

ALTER TABLE visiteur RENAME TO utilisateur ;

--
-- Ajout d'un champ dans utilisateur
--

ALTER TABLE utilisateur ADD COLUMN idType varchar(1) NOT NULL ;

--
-- Déclaration de tous les utilisateurs actuels comme des "visiteurs"
--

UPDATE utilisateur SET idType = 'V';

--
-- Déclaration de la clé étrangère "idType" en référence à "id" de la table "type"
--

ALTER TABLE utilisateur
ADD CONSTRAINT fk_type FOREIGN KEY (idType) REFERENCES type (id);

--
-- Ajout d'un champ dans utilisateur
--

ALTER TABLE fichefrais ADD COLUMN idTypeVehicule varchar(10);

--
-- Déclaration de la clé étrangère "idTypeVehicule" en référence à "id" de la table "typeVehicule"
--

ALTER TABLE `fichefrais`
  ADD CONSTRAINT `fk_TypeVehicule` FOREIGN KEY (`idTypeVehicule`) REFERENCES `typeVehicule` (`id`);